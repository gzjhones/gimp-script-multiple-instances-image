(define (script-fu-scale image layer instances samelong)
   (let* (
        (position 0)
        (value (cond ((= instances 0) 4) 
                     ((= instances 1) 9) 
	                  ((= instances 2) 16)
			            ((= instances 3) 25))
               ) 
        (range (sqrt value))
        (width (car (gimp-image-width image)))
        (height (car (gimp-image-height image)))
      )

      (gimp-context-push)
      
      ;Condition Same size image or multiply (width and height X instances)
      (cond ((= samelong TRUE)
            (set! width (/ width range))
            (set! height (/ height range))
         )
         (else (gimp-image-resize image (* range width) (* range height) 0 0))
      )
      ;New layer base
      (gimp-layer-scale layer width height TRUE)

      ;Two loops in base to sqtr of instances (4, 9, 16, 25)
      ;Rows
      (let loop ((i 0))
         (if (< i range)
         (begin
            ;Columns 
            (let loop ((j 0))
            (if (< j range)
            (begin
               (function-loop layer image width height i j position)
               (set! position (+ position 1))
            (loop (+ j 1) ) ) ) )
      (loop (+ i 1) ) ) ) )
      
      (gimp-context-pop)
		(gimp-displays-flush)
   )
)

(define (function-loop f_layer f_image f_width f_height i j p)
   (let* (
         (layer-type (car (gimp-drawable-type f_layer)))
         (layer-temp (car (gimp-layer-new f_image f_width f_height layer-type "temp1" 100 NORMAL-MODE)))
         (x 0)
         (y 0)
      )

      (gimp-drawable-fill layer-temp FILL-TRANSPARENT)
      (gimp-image-insert-layer f_image layer-temp 0 -1)
      (gimp-edit-copy f_layer)
      (gimp-floating-sel-anchor (car (gimp-edit-paste layer-temp 0)))

      ;Order and position
      (set! x (* i f_width))
      (set! y (* j f_height))

      (gimp-layer-translate layer-temp x y)
      ;Rename layer
      (gimp-item-set-name layer-temp (number->string p))
   )
)

(script-fu-register                                ; Name Script
   "script-fu-instances"                               ; Script name to register
   "<Image>/Script-Fu/Multiple Instances Image..." ; Menu view
   "Generate intances image"                       ; script description
   "gzjhones"                                      ; author
   "Copyright 2021 by gzjhones; GNU GPL"           ; copyright
   "2021-11-06"                                    ; date
   ""                                              ; type of image
   SF-IMAGE      "Image" 0 
   SF-DRAWABLE   "Layer" 0
   SF-OPTION "Instances" '("4" "9" "16" "25") 
   SF-TOGGLE "Same size image"  FALSE
)
