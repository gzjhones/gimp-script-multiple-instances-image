# Multiple-instances

[GIMP](http://gimp.org) script-fu for batch resizing images.

- Maximum height and width can be specified.
- Crops to layer mask.
- Optional padding in any color to the full maximum dimensions.

#### Installation

- Clone https://gitlab.com/gzjhones/gimp-script-multiple-instances-image.git or download in .zip
- Unzip and move the file MultipleInstance.scm to your GIMP scripts folder. You can find the location of the scripts folder by going to **Edit > Preferences > Folders > Scripts**.
- If GIMP is running click **Filters > Script-Fu > Refresh Scripts**.

<a id="usage"></a>

#### Usage

The script dialog can be accessed at: **Script-Fu > Multiple Instances Image...**.

- **Number Instances** - Select box the number instances, options 4, 9, 16, 25.
- **Same Size** - Check box if is reescaling or holding the same size of the image in base at the number of instances: `Note: Check box = false: reduce the quiality in once instance`.

#### Contributing
